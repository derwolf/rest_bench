#include <pistache/endpoint.h>
#include <pistache/router.h>
#include <iostream>
#include <string>

using namespace Pistache;


void handleEcho(const Rest::Request& request, Http::ResponseWriter response) {
    auto query = request.query();

    std::string text = "";

    if (query.has("text")) {
        text = query.get("text").get();
        response.send(Http::Code::Ok, "{\"text\":\"" + text + "\"}\n");
        return;
    }

    response.send(Http::Code::Bad_Request);
}



int main() {
    std::cout << "Serving on port 9080..." << std::endl;

    Address addr(Ipv4::any(), Port(9080));
    auto opts = Http::Endpoint::options().threads(8);

    Rest::Router router;
    Rest::Routes::Get(router, "/echo", Rest::Routes::bind(&handleEcho));

    Http::Endpoint server(addr);
    server.init(opts);
    server.setHandler(router.handler());
    server.serve();

    return 0;
}
