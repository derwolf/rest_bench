FROM alpine

RUN apk update && \
    apk add build-base && \
    apk add binutils && \
    apk add cmake && \
    apk add git && \
    git clone https://github.com/oktal/pistache.git && \
    cd pistache && \
    mkdir build && \
    cd build && \
    cmake .. && \
    make && \
    make install && \
    cd / && \
    git clone https://gitlab.com/derwolf/rest_bench.git && \
    cd /rest_bench && \
    mkdir build && \
    cd build && \
    cmake ../src && \
    make && \
    make install && \
    apk del build-base && \
    apk del binutils && \
    apk del cmake && \
    apk del git && \
    rm -rf /rest_bench && \
    rm -rf /pistache

EXPOSE 9080

CMD ["rstbench"]
